var globs = {};
globs.endPoint = "https://slack.com/api/";
globs.token = "xoxp-3966212417-4940157210-7204384148-ec6c54";
globs.channelID = "C06S4CN64";
globs.ceoID = "U03UE68CB";

globs.users = [];
globs.channels = [];
globs.today = new Date();

var channelModel = function(id,name){
    this.id = id;
    this.name = name;
}

var userModel = function(id,dn,name,pix){
    this.id = id;
    this.displayName = dn;
    this.name = name;
    this.pix = pix;
}

//Toggle button functionality
var mute = false;
$('.btn-toggle').click(function() {
    $(this).find('.btn').toggleClass('active'); 
    
    if(!$(".btn-toggle>button:first-child").is(".active")){
        //Turn off sounds
        mute = true;
    }else {
        mute = false;
    }
    
    if ($(this).find('.btn-success').size()>0) {
    	$(this).find('.btn').toggleClass('btn-success');
    }
            
    $(this).find('.btn').toggleClass('btn-default');
       
});



//Get the timestamp for 12 AM today
var startTime = new Date();
startTime.setHours(0);
startTime.setMinutes(0);
startTime.setSeconds(0);
startTime.setMilliseconds(0);


var getUserDisplayName = function(uid){
    for(var i =0;globs.users.length;i++) {
        if(globs.users[i].id === uid) {
            return globs.users[i].displayName;
        }
    }
};

var getChannelDisplayName = function(cid){
    for(var i=0;globs.channels.length; i++){
        if(globs.channels[i].id === cid){
            return globs.channels[i].name;
        }
    }
};

//Does our browser support notifications & websockets?
var notificationSupport = false;
var webSocketSupport = "WebSocket" in window;

var notificationHandle = window.Notification || window.mozNotification
    || window.webkitNotification;

if('undefined' === typeof notificationHandle){
    
}else {
    notificationSupport = true;
    notificationHandle.requestPermission(function(permission) {});
}

var notify = function(uid) {
    var txt = "@"+getUserDisplayName(uid)+"'s"+
        " new status:\n"+ $('#'+uid+' .status-text').text();
    var title = "CrowdAfrica status monitor";
    var options = {
        body: txt,
        tag: 'statusNote',
        icon: '../img/icon.png',
        dir: 'ltr'
    };
    
    var note = new notificationHandle(title,options);
    
    note.onclick = function() {
        console.log('notification.onclick()');
    };
    note.onerror = function(e) {
        console.log('notification.onerror()');
    };
    note.onshow = function() {
        console.log('notification.onshow()');
    };
    note.onclose = function() {
        console.log('notification.onclose()');
    };
    
    //play sound if page is not visible
    // Currently different browsers have different events
    var hidden, visibilityChange;
    if (typeof document.hidden !== 'undefined') {
        // Opera 12.10, Firefox >=18, Chrome >=31, IE11
        hidden = 'hidden';
        visibilityChangeEvent = 'visibilitychange';
    } else if (typeof document.mozHidden !== 'undefined') {
        // Older firefox
        hidden = 'mozHidden';
        visibilityChangeEvent = 'mozvisibilitychange';
    } else if (typeof document.msHidden !== 'undefined') {
        // IE10
        hidden = 'msHidden';
        visibilityChangeEvent = 'msvisibilitychange';
    } else if (typeof document.webkitHidden !== 'undefined') {
        // Chrome <31 and Android browser (4.4+ !)
        hidden = 'webkitHidden';
        visibilityChangeEvent = 'webkitvisibilitychange';
    }

    if (document[hidden] && mute === false) {
        console.log('Page is not visible\n');
        $('#note')[0].play;
    }
    
    /*if (typeof document.addEventListener === 'undefined' ||
             typeof document[hidden] === 'undefined'   ) {
            console.log('Page Visibility API isnt supported, sorry!');
        } else {
            //document.addEventListener(visibilityChangeEvent, visibleChangeHandler, false);
        }*/
    
    return true;
};

//Replace Slack IDs with names.
var removeIDs = function(text){
    for(var i=0;i < globs.channels.length;i++){
        text = text.replace("<#" + globs.channels[i].id + ">",
             "<span class='label label-info'>#" + globs.channels[i].name + "</span>");
    }
    
    for(var i=0;i < globs.users.length;i++){
        text = text.replace("<@" + globs.users[i].id + ">", 
            "<span class='label label-success'>@" + globs.users[i].displayName + "</span>");
    }
    
    return text;
}


var updateStatus = function(id,text,time){
    $('#'+id+' .status-text').html(removeIDs(text));
    $('#'+id+' .time').text("• Since "+time);
};

var putUpUser = function(user,vip){
    if(vip){
        $('#ceo_label').text("@"+user.displayName);
        $('#ceo_image').attr('src',user.pix);
        return;
    }
    var $row = $('<div class="userRecord row"></div>');
    $row.attr('id',user.id);
    var $writeUp = $('<div class="col-sm-10"></div>');
    var $status = $('<h3 class="status-text"></h3>');
    var $dn = $('<h4><span class="label label-default dn"></span></h4>');
    $dn.find('.dn').text("@"+user.displayName);
    var $time = $('<h4><small class="text-muted time"></small></h4>');
    
    $writeUp.append($status);
    $writeUp.append($dn);
    $writeUp.append($time);
    
    $row.append($writeUp);
    
    var $pixDiv = $('<div class="col-sm-2"></div>');
    var $pix = $('<a href="#" class="pull-right"><img src="'+
                 user.pix
                 +'" class="img-circle"></a>');
    $pixDiv.append($pix);
    
    $row.append($pixDiv);
    $('#team').append($row);
    
    //add divider
    $divider = $('<div class="row divider"><div class="col-sm-12"><hr></div> </div>');
    $('#team').append($divider);
    
};

var updatePresence = function(uid,active) {
    if(active) {
         $('#'+uid+' .label-default').css("background-color","#00923F");
    }else {
         $('#'+uid+' .label-default').css("background-color","#DDD");
    }
};

var getUserPresence = function(uid) {
    $.post(globs.endPoint+"users.getPresence",{"token":globs.token,"user": uid},function(d,status) {
        if(status === 'success') {
            if(d.ok) {
                updatePresence(uid,d.presence === "active");
            }
        }
    });
};

//Get what's on our channel
var pollChannel = function(){

    var data = {"token": globs.token,
                       "channel": globs.channelID,
                        "inclusive": 1,
                "oldest": startTime.getTime()/1000
                    };
    $.post(globs.endPoint+"channels.history",data,function(data,status) {
        if(status === "success"){
            var arr = [];
            if(data.ok === true){ //we got some
                var stObj = {};

                for(var i=0;i< data.messages.length;i++){
                    if(stObj[data.messages[i].user] === undefined ||
                      stObj[data.messages[i].user].ts
                           < data.messages[i].ts){
                        stObj[data.messages[i].user] = data.messages[i];
                    }

                }

                for(var i=0;i< globs.users.length;i++){
                    var uid = globs.users[i].id;
                    var usr = stObj[uid];
                    var usr = stObj[globs.users[i].id];
                    if(usr === undefined){
                        continue;
                    }

                    var date = new Date(Number(usr.ts.split(".")[0])*1000);

                    updateStatus(uid,usr.text,date.toString().split(" ")[4]);
                    
                    //Get users' presence info
                    getUserPresence(uid);
                }
            } 
        }
    });
       
};

//Get our channels
$.post(globs.endPoint+"channels.list", {"token": globs.token},function(data,status) {
    if(status === "success"){
        if(data.ok === true){ //we've got some
            for(var i=0;i < data.channels.length;i++){
                var channel = new channelModel(data.channels[i].id,data.channels[i].name);
                globs.channels.push(channel);
            }
        }
    }
});

//Get our users
$.post(globs.endPoint+"users.list",
       {"token": globs.token},function(data,status) {
    if(status === "success"){
        if(data.ok === true){ //we've got some
            for(var i=0;i < data.members.length;i++){
                if(data.members[i].deleted === true){
                    continue;
                }
                var user = new userModel(data.members[i].id,
                                    data.members[i].name,
                                     data.members[i].profile.real_name,
                                    data.members[i].profile.image_72);
                globs.users.push(user);
                
                //put up this user
                if(user.id === globs.ceoID){
                    putUpUser(user,true);
                }else{
                    putUpUser(user,false);
                }
            }
            pollChannel(); // we're sure of users at this point
        }
    }
});

//Good morning friends, now we start.
if(! webSocketSupport){
    alert("WebSocket not supported by browser.\nYou'll have to refresh from time to time to get current status updates");
}else{
    $.post(globs.endPoint+"rtm.start",{"token": globs.token},function(data,status) {
        if(status === 'success'){
            if(data.ok){ //Al-izz-well
                var socket = new WebSocket(data.url);
                socket.onopen = function() {};

                socket.onmessage = function(e) {
                    var meat =JSON.parse(e.data);
                    //console.log(meat);

                    if(meat.type === "presence_change") {
                        updatePresence(meat.user,meat.presence === "active");
                    }
                    else if(meat.type ===  "message") {
                        //if it ain't our channel, ignore
                        if(meat.channel === globs.channelID) {
                            var subtype = meat.subtype;
                            var date,userID,statusText;
                            
                            if(subtype === undefined){
                                date = new Date(Number(meat.ts.split(".")[0])*1000);
                                userID = meat.user;
                                statusText = meat.text;
                            }else if(subtype === "message_changed"){ //in case of edited status updates
                                date = new Date(Number(meat.message.edited.ts.split(".")[0])*1000);
                                userID = meat.message.user;
                                statusText = meat.message.text;
                            }else if(subtype === "message_deleted"){ //Not supported at the moment.
                                return;
                            }
                            //Put up the new status
                            updateStatus(userID,statusText,date.toString().split(" ")[4]);
                            //Notify the user
                            if(notificationSupport) {
                                notify(userID);
                            }
                        }
                    }
                };

                socket.onerror = function(e) {
                    console.log("We're vomitting: \n"+e);
                }
            }
        }
    });
}
